import collections

selections=collections.OrderedDict()

def getPreselections():
    selections["None"]="(1)"
    #missing met triggers and additional multijet cuts right now
    #(passMETtriggers)*(eT_miss_track>30)*(dphi_track<TMath::Pi()3)
    
    #very basic 1b-4j-0l preselection
    selections["pre-1b-4j-0l"]="(eT_miss>250)*(nj_good>=4)*(num_bjets>=1)*(nbaselineLep==0)*(pT_4jet>40)*(pT_2jet>80)*(dphimin3>0.4)*(dphimin2>0.4)*(passMETtriggers)"
 
    #preselection for non-compressed 2 body regions
    selections["pre-2b-4j-0l"]="(eT_miss>250)*(nj_good>=4)*(num_bjets>=2)*(nbaselineLep==0)*(pT_4jet>40)*(pT_2jet>80)*(dphimin4>0.4)*(passMETtriggers)*(metsigST>5)*(MTbmin>50)*(passtauveto)"
    selections["pre-2b-4j-1l"]="(eT_miss>250)*(nj_good>=4)*(num_bjets>=2)*(nbaselineLep==1)*(nEl+nMu==1)*(pT_4jet>40)*(pT_2jet>80)*(dphimin4>0.4)*(passMETtriggers)*(metsigST>5)*(MT<120)"

    ## Here are the preselection levels as Table 6 on 18th of July in the intnote with additional cut in C region on etmissTrack & dphiTrackOrig based on Alvaro's talk:
    ## https://indico.cern.ch/event/831351/contributions/3482467/attachments/1869937/3076562/CheckSRCMetSig.pdf
    selections["pre-2b-4j-0l-AB"]="(eT_miss>250)*(nj_good>=4)*(num_bjets>=2)*(nbaselineLep==0)*(pT_4jet>40)*(pT_2jet>80)*(dphimin4>0.4)*(passMETtriggers)*(metsigST>5)*(MTbmin>50)*(m_1fatjet_kt12>120)"
    selections["pre-1b-4j-0l-C"]="(eT_miss>250)*(nj_good>=4)*(num_bjets>=1)*(nbaselineLep==0)*(pT_4jet>40)*(pT_2jet>80)*(dphimin2>0.2)*(passMETtriggers)*(metsigST>5)*(pTbV1>40)*(pTjV4>40)*(dphi_track_orig<TMath::Pi()/3)*(eT_miss_track>30)"
    selections["pre-1b-4j-0l-C2"]="(eT_miss>250)*(nj_good>=4)*(num_bjets>=1)*(nbaselineLep==0)*(pT_4jet>40)*(pT_2jet>80)*(dphimin4>0.4)*(passMETtriggers)*(metsigST>5)*(pTbV1>40)*(pTjV4>40)"
    selections["pre-1b-4j-0l-CINT"]="(eT_miss>250)*(nj_good>=4)*(num_bjets>=1)*(nbaselineLep==0)*(pT_4jet>40)*(pT_2jet>80)*(dphimin2>0.2)*(passMETtriggers)*(metsigST>5)*(pTbV1>40)*(pTjV4>40)"

    selections["pre-2b-4j-2l"]="(nj_good>=4)*(num_bjets>=2)*(nbaselineLep==2)*(nEl+nMu==2)*(pT_2lep>20)*(pT_1lep>27)*(pT_4jet>40)*(pT_2jet>80)*(dphimin4>0.4)*(pass1Ltriggers)*(mll>40)"


    selections["pre-2b-4j-2lSF"]=selections["pre-2b-4j-2l"]+"*(nEl==2 || nMu==2)*(eT_miss_orig<50)*(eT_miss>100)"
 

    selections["pre-2b-5j-2l"]="(nEl+nMu==2)*(pT_1lep>30)*(pT_2lep>15)*(abs(charge_1l+charge_2l)==0)*(num_bjets>=2)*(nj_good==5)"
    selections["pre-2b-6j-2l"]="(nEl+nMu==2)*(pT_1lep>30)*(pT_2lep>15)*(abs(charge_1l+charge_2l)==0)*(num_bjets>=2)*(nj_good>=6)"

    selections["pre-0b-3j-3l"]="(nEl+nMu==3)*(pT_1lep>27)*(pT_2lep>20)*(pT_3lep>20)*(nj_good==3)*(abs(charge_1l+charge_2l+charge_3l)==1)*(num_bjets==0)"
 
    selections["pre-2b-4j-3l"]="((nEl+nMu)==3)*(nj_good>=4)*(pT_1lep>27)*(pT_3lep>20)*(num_bjets>=2)*(mll<101.2)*(mll>81.2)*(abs(charge_1l+charge_2l+charge_3l)==1)*(pass1Ltriggers==1)"
   

    selections["pre-2b-4j-2leOS"]="(nj_good>=4)*(num_bjets>=2)*(nbaselineLep==2)*(nEl==2)*(nMu==0)*(pass1etriggers)*(pT_1lep>27)*(pT_2lep>20)*(charge_1l+charge_2l==0)*(eT_miss>250)*(dphimin4_orig>0.2)"    

    # preselection for compressed 2-body regions
    selections["pre-1b-4j-0l-src"]="(eT_miss_orig>250)*(nj_good>=4)*(num_bjets>=1)*(nbaselineLep==0)*(pT_4jet>40)*(pT_2jet>80)*(dphimin4>0.4)*(passMETtriggers)*(metsigST>5)"
    selections["pre-1b-4j-1l-src"]="(eT_miss_orig>250)*(nj_good+nEl+nMu>=4)*(num_bjets>=1)*(nbaselineLep==1)*(nEl+nMu==1)*(pT_4jetLep>40)*(pT_2jetLep>80)*(dphimin4Lep>0.4)*(passMETtriggers)*(metsigST>5)*(pT_1lep>=20)"

    # SRD preselections 
     
    selections["pre-0l-soft"]="(passTightCleaning)*(passMETtriggers)*(eT_miss_orig>250)*(nbaselineLep==0)*(pT_1lightjet>=250)*(dphi_trk_ISRjMet>2.4)*(dphi_track_orig<TMath::Pi()/3)*(eT_miss_track>30)"
    selections["pre-1l-soft"]="(passTightCleaning)*(passMETtriggers)*(eT_miss_orig>250)*(nbaselineLep==1)*(nEl==1 || nMu==1)*(pT_1lightjet>=250)*(dphi_trk_ISRjMet>2.4)*(dphi_track_orig<TMath::Pi()/3)*(eT_miss_track>30)*(MT_orig<120)*(pT_1lep>=20)"
    selections["pre-2l-soft"]="(passTightCleaning)*(pass1Ltriggers)*(eT_miss>150)*(nbaselineLep==2)*(nEl==2 || nMu==2)*(pT_1lightjet>=200)*(pT_1lep>=30)*(pT_2lep>=20)"

    selections["pre-3l"]="(nEl+nMu>2)*(pT_2jet>80)*(pT_4jet>40)*(num_bjets>=2)"

def getRegionA():

    selections["pre-2b-4j-0l-tight"]=selections["pre-2b-4j-0l"]+"*(MTbmin>200)"
    selections["pre-2b-4j-0l-tight-TT"]=selections["pre-2b-4j-0l-tight"]+"*(m_1fatjet_kt12>120)*(m_2fatjet_kt12>120)"
    selections["pre-2b-4j-0l-tight-TW"]=selections["pre-2b-4j-0l-tight"]+"*(m_1fatjet_kt12>120)*(m_2fatjet_kt12>60)*(m_2fatjet_kt12<120)"
    selections["pre-2b-4j-0l-tight-T0"]=selections["pre-2b-4j-0l-tight"]+"*(m_1fatjet_kt12>120)*(m_2fatjet_kt12<60)"

    ########################################################
    # New SRA definition based on cathegories (SRA-ort-v5)
    selections["SRATT"]=selections["pre-2b-4j-0l-tight-TT"]+"*(MT2Chi2>450)*(m_1fatjet_kt8 > 60.00)*(metsigST > 25.00)*(flav_1fatjet_kt12 > 0.00)*(flav_2fatjet_kt12 > 0.00)*(dRb1b2 > 1.00)"
    selections["SRATW"]=selections["pre-2b-4j-0l-tight-TW"]+"*(MT2Chi2>450)*(m_1fatjet_kt8 > 60.00)*(metsigST > 25.00)*(flav_1fatjet_kt12 > 0.00)"
    selections["SRAT0"]=selections["pre-2b-4j-0l-tight-T0"]+"*(MT2Chi2>450)*(m_1fatjet_kt8 > 60.00)*(metsigST > 25.00)*(flav_1fatjet_kt12 > 0.00)"
    ########################################################

    selections["SRA"]='('+selections['SRATT']+')||('+selections['SRATW']+')||('+selections['SRAT0']+')'
    
def getRegionB():

    ##########################################################################################
    # Agreed SRB definition based on cathegories (SRB-PhHarmMT2-v3) with harmonised metsig cut
    ##########################################################################################
    selections["SRB_pre"] = selections["pre-2b-4j-0l"]+"*(passtauveto)*(MTbmin>200)*(MTbmax>200)*(dRb1b2>1.4)*(m_1fatjet_kt12>120)*(MT2Chi2<450)*(metsigST>14)"
    selections["SRBTT"] = selections["SRB_pre"]+"*(m_2fatjet_kt12>120)"
    selections["SRBTW"] = selections["SRB_pre"]+"*(m_2fatjet_kt12>60)*(m_2fatjet_kt12<120)"
    selections["SRBT0"] = selections["SRB_pre"]+"*(m_2fatjet_kt12<60)"
    selections["SRB"]='('+selections['SRBTT']+')||('+selections['SRBTW']+')||('+selections['SRBT0']+')'

    ##########################################################################################

def getRegionC():
    selections["SRC-dphi4"] = selections["pre-1b-4j-0l-src"]+"*(NjV>=4)*(NbV>=2)*(MS>400)*(pTbV1>40)*(dphiISRI>3.00)*(PTISR>400)*(pTjV4>50)*(metsigST>=5.)" 
    selections["SRC-dphi2"] = selections["SRC-dphi4"].replace("dphimin4>0.4","dphimin2>0.2")
    selections["SRC-dphi2-extra"] = selections["SRC-dphi2"]+"*(eT_miss_track>30)*(dphi_track<TMath::Pi()/3)"
    selections["VRC-test"] = selections["SRC-dphi2"]+"*(eT_miss_track>30)*(dphi_track>2.5)"
        
    selections["SRC"]= selections["SRC-dphi2-extra"]
    selections["SRC-1"]= selections["SRC"]+"*(RISR>=0.3)*(RISR<0.4)"
    selections["SRC-2"]= selections["SRC"]+"*(RISR>=0.4)*(RISR<0.5)"
    selections["SRC-3"]= selections["SRC"]+"*(RISR>=0.5)*(RISR<0.6)"
    selections["SRC-4"]= selections["SRC"]+"*(RISR>=0.6)*(RISR<0.7)"
    selections["SRC-5"]= selections["SRC"]+"*(RISR>=0.7)"
    selections["SRC-345"]= selections["SRC"]+"*(RISR>=0.5)"

    selections["SRC1_5"]= selections["SRC"]+"*(RISR>=0.3)*(RISR<=0.8)"
     
    selections["SRC_alt"] = selections["SRC-dphi4"]
    selections["SRC_alt-1"]= selections["SRC_alt"]+"*(RISR>=0.3)*(RISR<0.4)"
    selections["SRC_alt-2"]= selections["SRC_alt"]+"*(RISR>=0.4)*(RISR<0.5)"
    selections["SRC_alt-3"]= selections["SRC_alt"]+"*(RISR>=0.5)*(RISR<0.6)"
    selections["SRC_alt-4"]= selections["SRC_alt"]+"*(RISR>=0.6)*(RISR<0.7)"
    selections["SRC_alt-5"]= selections["SRC_alt"]+"*(RISR>=0.7)"

def getRegionD():
    selections["SRD0"]=selections["pre-0l-soft"]+"*(htSig>=26)*(nbtrackjets>0)*(num_bjets==0)*(dphibb_trk<2.5)*(pT_1btrackjet<50)*(fabs(eta_1btrackjet)<1.2)*(dphimin4>0.4)*(dphibtrkJISRmax>2.2)*(!hasVROverlap)"
    selections["SRD1"]=selections["pre-0l-soft"]+"*(pT_1btrackjet>10)*(htSig>=22)*(nbtrackjets>0)*(num_bjets==1)*(pT_1trackjet<40)*(dphibjet1JISR>2.2)*(fabs(eta_1bjet)<1.6)*(dphimin4_trk>1.2)*(!hasVROverlap)"
    selections["SRD2"]=selections["pre-0l-soft"]+"*(htSig>22)*(num_bjets>=2)*(dphibjet1JISR>2.2)*(dphibjet2JISR>1.6)*(pT_1bjet<175)*(pT_1bjet>0)*(fabs(eta_2bjet)<1.2)"

    selections["bbMET-SRC"]="(nbaselineLep==0)*(passMETtriggers)*(leadb1 != 1)*(num_bjets==2)*(pT_1jet>700)*(eT_miss>650)*(A>0.86)*(ht3<80)*(mjj>350)*(nj_good<=5)"



getPreselections()
getRegionA()
getRegionB()
getRegionC()
getRegionD()

