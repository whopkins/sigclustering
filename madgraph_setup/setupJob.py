#!/usr/bin/env python

import argparse, re

def createGenFile(stopMass, lspMass, slhaTemplate, nevents=50000):
    """!
    Create a text file that has all the Madgraph commands needed to produce 
    a sample with pair production of stops and where the stop
    decays to a top+neutralino.
    """
    stopPDGID = '1000006'
    lspPDGID = '1000022'

    lhaFName = 'stopLSP_'+str(stopMass)+'_'+str(lspMass)+'.slha'
    stopMassRE = re.compile(stopPDGID+'\s+(\d+\.*\d*)')
    lspMassRE = re.compile(lspPDGID+'\s+(\d+\.*\d*)')

    lhaFile = open(lhaFName, 'w')
    with open(slhaTemplate) as tempF:
        massSearch = False
        for inLine in tempF.readlines():
            outline = inLine
            if 'BLOCK MASS' in inLine:
                massSearch = True;
            if 'Block msd2' in inLine:
                massSearch = False
            if massSearch:
                stopMatch = stopMassRE.search(inLine)
                lspMatch = lspMassRE.search(inLine)
                if stopMatch:
                    templateMass  = stopMatch.group(1)
                    newMass = str(stopMass)
                    outline = inLine.replace(templateMass, newMass)
                if lspMatch:
                    templateMass  = lspMatch.group(1)
                    newMass = str(lspMass)
                    outline = inLine.replace(templateMass, newMass)
                    
            lhaFile.write(outline)

    lhaFile.close()
    
    outStr = (
        'import model MSSM_SLHA2\n'
        'define fu = u c e+ mu+ ta+\n'
        'define fu~ = u~ c~ e- mu- ta-\n'
        'define fd = d s ve~ vm~ vt~\n'
        'define fd~ = d~ s~ ve vm vt\n'
        'define susylq = ul ur dl dr cl cr sl sr\n'
        'define susylq~ = ul~ ur~ dl~ dr~ cl~ cr~ sl~ sr~\n'

        'generate p p > t1 t1~ $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @1\n'
        'add process p p > t1 t1~ j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @2\n'
        'add process p p > t1 t1~ j j $ go susylq susylq~ b1 b2 t2 b1~ b2~ t2~ @3\n'
        'output stop2topLSP_{stopMass}_{lspMass}\n'
        'launch stop2topLSP_{stopMass}_{lspMass}\n'
        'shower=Pythia8\n'
        #'done\n'
        'set nevents {nevents}\n'
        'set pdgs_for_merging_cut 1, 2, 3, 4, 21, 1000001, 1000002, 1000003, 1000004, 1000021, 2000001, 2000002, 2000003, 2000004\n'
        'set Merging:Process pp>{{t1,1000006}}{{t1~,-1000006}}\n'
        '{lhaFName}\n'
        'update missing\n\n'
    )
    outStr = outStr.format(stopMass=str(stopMass), lspMass=str(lspMass), lhaFName=lhaFName, nevents=nevents)
    outF = open('stopLSP_'+str(stopMass)+'_'+str(lspMass)+'.run', 'w')
    outF.write(outStr)
    outF.close()

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='Create and submit stop job')
    parser.add_argument('stopMass', type=int, help='The stop mass in GeV.')
    parser.add_argument('lspMass', type=int, help='The LSP mass in GeV.')
    parser.add_argument('-s', '--slhaTemplate', type=str, help='Path to SLHA file.', default='param_card_stop_tN_1300_1.dat')
    args = parser.parse_args()
    createGenFile(args.stopMass, args.lspMass, args.slhaTemplate)
